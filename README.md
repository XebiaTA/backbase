# Protractor Demo for Backbase

## TODO
* [x]setting up some tests
* [x]doing some image comparison
* [x]showing some Page Objects Model - magic
* [x]connecting with SauceLabs
* [x]connecting with BrowserStack
* [ ]fixing 'NoSuchElementError: No element found using locator: By(link text, Demo request)' on BrowserStack
* [ ]report back issues


## Requirements
- npm 12.1.0 (node.js package manager)
- latest chrome

## Installing
run `npm install` to install all dependencies

## Running Locally
run the tests locally with `npm run e2e`.

### Running on SauceLabs
to run the tests on SauceLabs:
```
export SAUCE_USERNAME='<your_saucelabs_username>
export SAUCE_ACCESS_KEY='<your_saucelabs_access_key>'
```

`npm run sauce`

### Running on BrowserStack
to run the tests on BrowserStack:

```
export BROWSERSTACK_USERNAME='<your_browserstack_username>'
export BROWSERSTACK_ACCESS_KEY='<your_browserstack_accesskey>'
```

`npm run sauce`

## Issues
If you run the test against saucelabs, you will run into a timeout.
The tests will run fine, the results are ok but saucelabs will still mark the test as 'failed' because of this protractor issue:
https://github.com/angular/protractor/issues/4817

## Debugging tests
https://medium.com/@ganeshsirsi/how-to-debug-protractor-tests-in-visual-studio-code-e945fc971a74
 
## Issues I found
- In the 'Become a Backbase partner, the input fields are selected with non-descriptive id's. I would make them descriptive for the sake of communication and testability
- form is being send unvalidated. I emptied the form and aftr the submit still got the 'thank you' - message.

## Tools I used
* Visual Studio Code
* iTerm2
* Firefox Developer Edition
