var Partner = function() {

    var PartnerURL = 'https://www.backbase.com/about/partners/'
    var modalTitle = element(by.cssContainingText('.title.default', 'Become a Backbase partner'))

    //for the sake of variation I choose to use by.LinkText and not by ID. Where possible you should always use by ID
    var partnerButton = element(by.linkText('Become a partner'));

    this.getURL = function() {
        return PartnerURL;
    },

    this.getPartnerButton = function() {
        return partnerButton;
    }

    this.getModalTitle = function() {
        return modalTitle.getText();
    }
  };
  module.exports = new Partner();
