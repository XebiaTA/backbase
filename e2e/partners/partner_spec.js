import {$, browser} from 'protractor';

var partner = require('./partner_pom');

describe('checking the workingz of the Becoming a partner - functionals', async () => {
    beforeEach( () => {
        browser.waitForAngularEnabled(false);
        browser.get(partner.getURL());
        browser.ignoreSynchronization = true;
    });

    it('should have a title', () => {
        expect(browser.getTitle()).toEqual('Partners | Backbase');
    });

    it ('tests the Become a Backbase partner - flow', async () => {
        // browser.ignoreSynchronization = true;
        //click on 'Find a partner' - button
        await partner.getPartnerButton().click();

        expect(partner.getModalTitle()).toEqual('Become a Backbase partner');
        element(by.tagName("select#input_13_1")).click();
        element(by.tagName("select#input_13_1 [value='Referral Partner']")).click();
        element(by.id('input_13_2')).sendKeys('Joël');
        element(by.id('input_13_3')).sendKeys('Grimberg');
        element(by.id('input_13_4')).sendKeys('jgrimberg@xebia.com');
        element(by.id('input_13_5')).sendKeys('0615022943');
        element(by.id('input_13_6')).sendKeys('QA Engineer');
        element(by.id('input_13_7')).sendKeys('Xebia Digital Assurance');
        element(by.id('input_13_11')).sendKeys('The Xebia - address');
        element(by.id('input_13_12')).sendKeys('Hilversum, NL');
        element(by.id('input_13_13')).sendKeys('The ZIP');
        element(by.id('input_13_14')).sendKeys('https://www.xebia.com');
        element(by.id('gform_submit_button_13')).submit();
        browser.sleep(100000);
        await browser.imageComparison.saveScreen('becoming_short', { /* some options*/ });

        

        // await browser.imageComparison.saveScreen('submitted_form', { /* some options*/ });
 //       expect(await browser.imageComparison.checkScreen('becoming_short', { /* some options*/ })).toEqual(0);
    
        // expect(await browser.imageComparison.checkscreen('submitted_form', { /* some options*/ })).toEqual(0);

      
    }, 100000);

});