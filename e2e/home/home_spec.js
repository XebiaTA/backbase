import {$, browser} from 'protractor';

var home = require('./home_pom');

describe('Demo: Backbase Site ', async() => {
  beforeEach( () => {
    browser.waitForAngularEnabled(false);
    browser.get(browser.baseUrl);
  });

  it('should have a title', () => {
    expect(browser.getTitle()).toEqual('Backbase - Omni-Channel Digital Banking. Delivered.');
  });

  it('should have a logo with the right link', () => {
    expect(home.getLogoLink()).toEqual(browser.baseUrl);
  });

  it('is able to request a demo', async () => {
    await home.getDemoButton().click()

    const EC = protractor.ExpectedConditions;
    browser.wait(EC.urlContains('https://www.backbase.com/resources/request-live-demo-2-2/'), 5000); 
  });

  it('should save some screenshots', async() => {
    await browser.imageComparison.saveScreen('examplePaged', { /* some options*/ });
      
    await browser.imageComparison.saveFullPageScreen('fullPage', { /* some options*/ });
  },100000);
    
  it('should compare successful with a baseline', async() => {
    expect(await browser.imageComparison.checkScreen('examplePaged', { /* some options*/ })).toEqual(0);
    
    expect(await browser.imageComparison.checkFullPageScreen('fullPage', { /* some options*/ })).toEqual(0);
  },100000);

});