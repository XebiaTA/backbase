var Home = function() {
    var logoLink = element(by.css('.logo__link'));    
    var demoButton = element(by.linkText('Demo request'));

    this.getLogoLink = function() {
        return logoLink.getAttribute('href');
    }

    this.getDemoButton = function() {
        return demoButton;
    }
  };
  module.exports = new Home();
