const { join } = require('path');
exports.config = {
    plugins: [
        {
            // The module name
            package: 'protractor-image-comparison',
            // Some options, see the docs for more
            options: {
                baselineFolder: join(process.cwd(), './baseline/'),
                formatImageName: `{tag}-{logName}-{width}x{height}`,
                screenshotPath: join(process.cwd(), '.tmp/'),
                savePerInstance: true,
                autoSaveBaseline: true
            },
        },
    ],
    framework: 'jasmine',
    allScriptsTimeout: 300000,
    baseUrl: 'https://www.backbase.com/',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    multiCapabilities: [{
        browserName: 'firefox',
        'moz:firefoxOptions': {
            // 'args': ['--safe-mode','--headless'],
            'args': ['--headless'],
          }
        
    //   }, {
    //     browserName: 'chrome',
    //     shardTestFiles: true,
    //     chromeOptions: {
    //         'args': ['start-minimized', 'window-size=1920,1080', 'headless', 'disable-browser-side-navigation']
    //     }
    }],

    suites: {
        // home: 'e2e/home/**/*_spec.js',
        partners: 'e2e/partners/**/*_spec.js',
        marketplace: 'e2e/marketplace/*_spec.js'
      },
};