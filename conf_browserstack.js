var browserstack = require('browserstack-local');
const { join } = require('path');

exports.config = {
    plugins: [
        {
            // The module name
            package: 'protractor-image-comparison',
            // Some options, see the docs for more
            options: {
                baselineFolder: join(process.cwd(), './baseline/'),
                formatImageName: `{tag}-{logName}-{width}x{height}`,
                screenshotPath: join(process.cwd(), '.tmp/'),
                savePerInstance: true,
                autoSaveBaseline: true
            },
        },
    ],
    framework: 'jasmine',
    baseUrl: 'https://www.backbase.com/',

    specs: ['e2e/*.js'],

    // restartBrowserBetweenTests: true,

    onPrepare: function () {
        var caps = browser.getCapabilities()
    },

    'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',

    'capabilities': {
        'browserstack.user': process.env.BROWSERSTACK_USERNAME,
        'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY,
        'browserstack.local': true,
        'browserstack.debug': true,
        'browserName': 'chrome',
        'name': 'Bstack-[Protractor] Local Test'
    },

    // Code to start browserstack local before start of test
    beforeLaunch: function(){
        console.log("Connecting local");
        return new Promise(function(resolve, reject){
        exports.bs_local = new browserstack.Local();
        exports.bs_local.start({'key': exports.config.capabilities['browserstack.key'] }, function(error) {
            if (error) return reject(error);
            console.log('Connected. Now testing...');

            resolve();
        });
        });
    },

    // Code to stop browserstack local after end of test
    afterLaunch: function(){
        return new Promise(function(resolve, reject){
        exports.bs_local.stop(resolve);
        });
    }
};